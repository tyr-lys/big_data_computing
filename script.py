import sys
import time
import numpy as np
import argparse
import warnings

import os
import multiprocessing as mp


class PhredColumnCalculator:

    def __init__(self):
        phred_arguments = self.input_arguments_phred()
        self.core_count = phred_arguments.n
        self.input_file = phred_arguments.fastqfile
        self.output_csv = phred_arguments.outputfile
        self.file_end = os.stat(self.input_file).st_size
        self.total_fragment_size = int(self.file_end/self.core_count[0])*self.core_count[0]
        self.process_initiator()

    @staticmethod
    def input_arguments_phred():
        """
        Gives arguments and handles the input of the arguments.
        :return: All given arguments as a list, even single values.
        """
        parser = argparse.ArgumentParser()
        parser.add_argument("fastqfile", help="Fastq file that is read and processed.")
        parser.add_argument("outputfile", nargs="?", default=None, help="(Optional) if filled in it should be the last "
                                                                        "mentioned file.")
        parser.add_argument("-n", nargs=1, help="Specify amount of threads to be used by the script.", type=int,
                            default=[1])

        collected_arguments = parser.parse_args()
        return collected_arguments

    def chunker(self):
        # file_end = os.stat(self.input_file).st_size
        bytes_per_chunk = int(self.file_end / self.core_count[0])
        for chunk_start in range(0, self.file_end, bytes_per_chunk):
            yield chunk_start, bytes_per_chunk

    def process_initiator(self):
        starttime = time.time()
        count_result = mp.Queue()
        value_result = mp.Queue()
        jobs = []
        for start_chunk, byte_chunk in self.chunker():
            if start_chunk + byte_chunk == self.total_fragment_size:
                p = mp.Process(target=self.read_it, args=(start_chunk, self.file_end, count_result, value_result))
                jobs.append(p)
                p.start()
                break
            else:
                p = mp.Process(target=self.read_it, args=(start_chunk, start_chunk + byte_chunk, count_result, value_result))
                jobs.append(p)
                p.start()
        for index, job in enumerate(jobs):
            print("Job {}, process: {}".format(index, job.pid))
            job.join()
        self.calculate_results(count_result, value_result)

        print(time.time()-starttime)

    def calculate_results(self,counted_occurrences, counted_values):
        count_length = counted_occurrences.qsize()
        counts = np.zeros(110)
        values = np.zeros(110)
        for index in range(count_length):
            counts += counted_occurrences.get()
            values += counted_values.get()
        warnings.filterwarnings("ignore", category=RuntimeWarning)
        result = (values/counts)-33
        if self.output_csv is None:
            print(result)
        # else:
        #     with open(self.output_csv, "w") as output_csv:
        #         for average_value in result:
        #             print(average_value, ",", sep="", file=output_csv)


    def read_it(self,start, stop, cq, vq):
        lachje = True
        on_track = False
        prup = []
        store_values = np.zeros(110, dtype="uint32")
        store_counts = np.zeros(110, dtype="uint32")
        with open(self.input_file, "rb") as miep:
            miep.seek(start)
            while lachje:
                try:
                    if miep.tell() >= stop:
                        lachje = False
                    else:
                        line = miep.readline()
                        if on_track:
                            dna = miep.readline().strip(b"\n")
                            miep.readline()
                            phred = miep.readline().strip(b"\n")
                            phredlen = len(phred)
                            if len(dna) == phredlen:
                                store_values[:phredlen] += (np.fromstring(phred, dtype="uint8"))
                                store_counts[:phredlen] += np.ones(phredlen, dtype="uint8")
                        elif line.startswith(b"@") and b" " in line:
                            on_track = True
                            dna = miep.readline().strip(b"\n")
                            miep.readline()
                            phred = miep.readline().strip(b"\n")
                            phredlen = len(phred)
                            if len(dna) == phredlen:
                                store_values[:phredlen] += (np.fromstring(phred, dtype="uint8"))
                                store_counts[:phredlen] += np.ones(phredlen, dtype="uint8")
                        else:
                            prup.append(line) # store broken lines, currently not handled.
                except StopIteration:
                    cq.put(store_counts)
                    vq.put(store_values)
                    break
        cq.put(store_counts)
        vq.put(store_values)
        return


def main(argv=None):
    if argv is None:
        argv = sys.argv
    PhredColumnCalculator()

    return 0

if __name__ == "__main__":
    sys.exit(main())