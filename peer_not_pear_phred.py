import sys
import numpy as np
import socket
import os

class PeerFileProcessor():

    def __init__(self, peers, fila):
        self.input_file = fila
        self.file_end = os.stat(fila).st_size
        self._byte_fragments = []
        self._sorted_total_peers = peers
        self._divided_fragments_per_host = {}
        self.store_values = np.zeros(110, dtype="uint32")
        self.store_counts = np.zeros(110, dtype="uint32")
        self._pre_fragmenter()
        start_chunk, stop_chunk = self._divided_fragments_per_host[socket.gethostbyname(socket.gethostname())]
        self._read_it(start_chunk, stop_chunk, fila)




    def _pre_fragmenter(self):
        bytes_per_chunk = int(self.file_end / len(self._sorted_total_peers))
        total_fragment_size = bytes_per_chunk * len(self._sorted_total_peers)
        for start_chunk, peer in zip(range(0, self.file_end, bytes_per_chunk), self._sorted_total_peers):
            if start_chunk + bytes_per_chunk == total_fragment_size:
                self._divided_fragments_per_host[peer] = (start_chunk, start_chunk + bytes_per_chunk)
                break
            else:
                self._divided_fragments_per_host[peer] = (start_chunk, start_chunk + bytes_per_chunk)
        return

    def _read_it(self,start, stop, input_file):
        lachje = True
        on_track = False
        prup = []
        with open(input_file, "rb") as miep:
            miep.seek(start)
            while lachje:
                try:
                    if miep.tell() >= stop:
                        lachje = False
                    else:
                        line = miep.readline()
                        if on_track:
                            dna = miep.readline().strip(b"\n")
                            next(miep)
                            phred = miep.readline().strip(b"\n")
                            phredlen = len(phred)
                            if len(dna) == phredlen:
                                self.store_values[:phredlen] += (np.fromstring(phred, dtype="uint8"))
                                self.store_counts[:phredlen] += np.ones(phredlen, dtype="uint8")
                        elif line.startswith(b"@") and b" " in line:
                            on_track = True
                            dna = miep.readline().strip(b"\n")
                            next(miep)
                            phred = miep.readline().strip(b"\n")
                            phredlen = len(phred)
                            if len(dna) == phredlen:
                                self.store_values[:phredlen] += (np.fromstring(phred, dtype="uint8"))
                                self.store_counts[:phredlen] += np.ones(phredlen, dtype="uint8")
                        else:
                            prup.append(line)  # store broken lines, currently not handled.
                except StopIteration:
                    return
        return

    def get_counts(self):
        return self.store_counts

    def get_values(self):
        return self.store_values

def main(argv= None):
    if argv is None:
        argv = sys.argv


if __name__ == "__main__":
    sys.exit(main())