import sys
import socket
import threading
import time


class PeerBroadcaster():
    def __init__(self):
        self.udp_port = 22934
        self._host_names = set()
        self._connection_attempts = 3
        self._used_clients = 4
        # UDP socket
        self._UDP_observer = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # socket options
        self._UDP_observer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._UDP_observer.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        # UDP BIND
        self._UDP_observer.bind(("", self.udp_port))
        self._peers()

    def _peers(self):
        """
        Start broadcasting to fellow peers and receiving fellow peers.
        """
        self._contact_peers = threading.Thread(target=self._contact_fellow_peers, args=())
        self._receive_peers = threading.Thread(target=self._receive_fellow_peers, args=())
        self._contact_peers.start()
        self._receive_peers.start()

    def _contact_fellow_peers(self):
        """
        Broadcasts a signal each second, to find fellow peers.
        """
        for attempt in range(self._connection_attempts):
            time.sleep(1)
            if len(self._host_names) < (self._used_clients+1) and attempt == self._connection_attempts:
                print("Could not find all host names, stop broadcasting.")
                break
            else:
                # Send host name
                self._UDP_observer.sendto(socket.gethostbyname(socket.gethostname()).encode("ascii"), ('<broadcast>', self.udp_port))

    def _receive_fellow_peers(self):
        """
        Receive signals from fellow peers and store their host names.
        """
        while True:
            # Avoid adding machine connection from itself, it would instantiate a useless client.
            fellow_client_UDP_data = self._UDP_observer.recv(512)
            # if fellow_client_UDP_data == socket.gethostbyname(socket.gethostname()).encode("ascii"):
            #     pass
            # else:
            if len(self._host_names) == self._used_clients:
                break
            else:
                self._host_names.add(fellow_client_UDP_data.decode("ascii"))

    def get_host_names(self):
        """
        Waits until all the hosts found each other and then returns their names.
        :return: Give back the host names as a set.
        """
        while self._receive_peers.is_alive() and self._contact_peers.is_alive():
            pass
        return sorted(list(self._host_names))

def main(argv=None):
    if argv is None:
        argv = sys.argv
    worst = PeerBroadcaster()
    worst.get_host_names()


if __name__ == "__main__":
    sys.exit(main())
