#!/usr/bin/env bash

#!/bin/bash
#SBATCH -- time 0:04:00
#SBATCH --job-name=test
#SBATCH -- nodes=4
#SBATCH -- cpus-per-task=1
#SBATCH -- mem=4096
#SBATCH -- partition=short


module load Python/2.7.11-foss-2016a numpy/1.9.2-foss-2016a-Python-2.7.11

python /home/p225083/sylt/script.py /home/p225083/rnaseq.fastq