import socket
import numpy as np
import sys
import threading
import time
import pickle
import random
import peer_broadcast
import peer_not_pear_phred

########################################################################################################################
# Currently unable to share data between peers and unable to shut them down when required.
# Data of a server_peer does not correspond correctly with all of its peer_clients and gives out of sync messages to a server_peer.
# If you wish to run it start each script very quickly after each other on separate machines otherwise the broadcast is unable to find the other peers.
########################################################################################################################


class MasterServer:
    def __init__(self,peers):
        self.peers= peers
        self._peers_with_connections = {}
        self._host_name = socket.gethostbyname(socket.gethostname())
        self.socket_for_peer = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket_for_peer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket_for_peer.bind((socket.gethostname(), 22934))
        self.socket_for_peer.listen(5)
        self.server_object = []
        self._processed_data = []
        self._solution_solver = False
        self._hostname = socket.gethostbyname(socket.gethostname())
        threading.Thread(target=self._server_receiver).start()
        threading.Thread(target=self._server_sender).start()
        threading.Thread(target=self._process_file).start()
        while True:
            connection, address = self.socket_for_peer.accept()
            self._peers_with_connections[address[0]] = connection

    def _server_receiver(self):
        """
        Receive data from all clients.
        """
        while True:
            if len(self._peers_with_connections) == 0:
                pass
            else:
                try:
                    ######################
                    # Serious issues here.#
                    ######################
                    for peer in self._peers_with_connections:
                        data = self._peers_with_connections[peer].recv(8192)
                        kaas = pickle.loads(data)
                        if data == b"":
                            pass
                        elif kaas[1] == b"Done":
                            if len(self._processed_data) == 0:
                                self._processed_data.extend(kaas[2])
                                self._solution_solver = True
                                # threading.Thread(target=self._combinining_results()).start()
                            elif len(self._processed_data) > 2 and kaas[-1] is False:
                                self._processed_data.extend(kaas[2])
                                sys.exit("Shuting down machine, end of process")
                            else:
                                sys.exit("Shutting down unknown")
                        else:
                            # No valid data sent
                            pass
                except RuntimeError:
                    # Just building up and retry.
                    pass

    def _server_sender(self):
        """
        Sends data to all clients and servers.
        """

        while True:
            time.sleep(1)
            message = pickle.dumps([socket.gethostname(), self.server_object,self._solution_solver])
            for connection in self._peers_with_connections:
                self._peers_with_connections[connection].send(message)

    def _process_file(self):

        while len(self._peers_with_connections) != 4:
            time.sleep(1)
            try:
                pass
            except RuntimeError:
                pass
        worst = peer_not_pear_phred.PeerFileProcessor(self.peers,sys.argv[1])
        self.server_object.append(worst.get_values())
        self.server_object.append(worst.get_values())
        print("Finished processing")
        return

    def _combinining_results(self):
        print(self._processed_data)
        while len(self._peers_with_connections) != 4 and len(self._processed_data) != 8:
            time.sleep(1)
            try:
                pass
            except RuntimeError:
                pass
        counts = np.zeros(110)
        values = np.zeros(110)
        for index in range(0,len(self._processed_data),2):
            counts += self._processed_data[index]
            values += self._processed_data[index+1]
        print((values / counts) - 33)


class Client:
    def __init__(self, address):
        """
        Creates a client object which can do some sending and receiving to a server.
        :param address: host address for which to connect to.
        """
        socket_for_peer = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        socket_for_peer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._done_processing = False
        self._own_server_processed_data = None
        self._not_solving = None
        self._host_name = socket.gethostbyname(socket.gethostname())
        try:
            socket_for_peer.connect((address, 22934))
        except ConnectionRefusedError:
            for attempt in range(2):
                time.sleep(2)
                print("Failed connection to {}, retrying.".format(address))
                try:
                    socket_for_peer.connect((address, 22934))
                except OSError:
                    break
        threading.Thread(target=self._client_sender, args=(socket_for_peer, address)).start()
        threading.Thread(target=self._client_receiver, args=(socket_for_peer,)).start()

    def _client_sender(self, socket_for_peer, address):
        """
        Sends data to all servers.
        :param socket_for_peer: The socket object to which the client has to connect to.
        :param address: The peer address to which the client connects to.
        """

        ######################
        #Serious issues here.#
        ######################
        while True:
            try:
                time.sleep(1)
                if self._done_processing:
                    time.sleep(random.randint(1,5))
                    socket_for_peer.send(pickle.dumps([self._host_name, b"Done", self._own_server_processed_data, self._not_solving]))
                else:
                    pass


            except (BrokenPipeError, ConnectionRefusedError, OSError):
                print("Lost connection with : {}.".format(address))
                break

    def _client_receiver(self, receiving_socket):
        """
        ONLY receives data from the master of the same machine.
        :param receiving_socket:
        """
        while True:
            try:
                data = receiving_socket.recv(8192)
                if data == b"":
                    pass
                else:
                    # SERVER DATA
                    # It only update the client that supports the master not the other clients.
                    data = pickle.loads(data)
                    if len(data[1]) == 2:
                        self._done_processing = True
                        if data[2] == 0:
                            self._not_solving = False
                        else:
                            self._not_solving = True
                        self._own_server_processed_data = data[1]


            except ConnectionResetError:
                sys.exit("Machine is shutdown.")


def main(argv=None):
    if argv is None:
        argv = sys.argv

    # Broadcasting
    kaas = peer_broadcast.PeerBroadcaster()
    peers = kaas.get_host_names()
    print(peers)

    try:
        print("starting server")
        ma = threading.Thread(target=MasterServer, args=(peers,))
        ma.start()
    except KeyboardInterrupt:
        sys.exit("Shutting server down by interruption.")
    connected_peers = []
    targets = False
    while True:
        if targets:
            break
        try:
            time.sleep(random.randint(5, 15))
            for peer in peers:
                if targets:
                    break
                try:
                    if len(connected_peers) == 4:
                        targets = True
                    if peer in connected_peers:
                        pass
                    else:
                        ######################
                        # Serious issues here.#
                        ######################
                        connected_peers.append(peer)
                        cl = threading.Thread(target=Client, args=(peer,))
                        cl.start()

                except ConnectionRefusedError:
                    pass
        except KeyboardInterrupt:
            sys.exit(0)


if __name__ == "__main__":
    main()
