import socket
import sys
import pickle
import numpy as np


class Client():

    def __init__(self):
        self.client()



    def client(self):
        # Fill in server name.
        host = "localhost"
        port = 10000

        # Connect to master/server
        s = socket.socket()
        s.connect((host, port))
        # Keep requesting data, if certain codes are sent ignore or shutdown the client.
        while True:
            data = s.recv(8192)

            if data == b"_----_":
                pass
            elif len(data) == 0 or data == b"---____-__-":
                break
            else:
                if data.startswith(b"_") and data.endswith(b"_"):
                    pass
                else:
                    # Unpickle the objects for processing
                    print("Working on data.")
                    start_stop, given_file = pickle.loads(data)
                    # Process the data.
                    lala = self.read_it(start_stop[0], start_stop[1], given_file)
                    # Send pickled objects back.
                    s.send(pickle.dumps(lala))
                    print("Send processed data back to server.")
        # If somehow the connection stops, stop the client.
        print("Killing client")
        s.close()

    def read_it(self,start, stop, input_file):
        lachje = True
        on_track = False
        prup = []
        store_values = np.zeros(110, dtype="uint32")
        store_counts = np.zeros(110, dtype="uint32")
        with open(input_file, "rb") as miep:
            miep.seek(start)
            while lachje:
                try:
                    if miep.tell() >= stop:
                        lachje = False
                    else:
                        line = miep.readline()
                        if on_track:
                            dna = miep.readline().strip(b"\n")
                            next(miep)
                            phred = miep.readline().strip(b"\n")
                            phredlen = len(phred)
                            if len(dna) == phredlen:
                                store_values[:phredlen] += (np.fromstring(phred, dtype="uint8"))
                                store_counts[:phredlen] += np.ones(phredlen, dtype="uint8")
                        elif line.startswith(b"@") and b" " in line:
                            on_track = True
                            dna = miep.readline().strip(b"\n")
                            next(miep)
                            phred = miep.readline().strip(b"\n")
                            phredlen = len(phred)
                            if len(dna) == phredlen:
                                store_values[:phredlen] += (np.fromstring(phred, dtype="uint8"))
                                store_counts[:phredlen] += np.ones(phredlen, dtype="uint8")
                        else:
                            prup.append(line)  # store broken lines, currently not handled.
                except StopIteration:
                    return store_counts, store_values
        return store_counts, store_values


def main(argv=None):
    if argv is None:
        argv = sys.argv
    # Let the client run on itself.
    Client()
    return 0


if __name__ == "__main__":
    main()
