import socket
import os
import sys
import threading
import time
import pickle
import numpy as np



class MasterServer:

    def __init__(self):
        # Fill in server name.
        host = "localhost"
        port = 10000
        self.setup_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.setup_socket.bind((host, port))
        self.setup_socket.listen(5)
        self.connection_list = []
        self.address_list = []
        self.counted_list = []
        self.value_list = []
        print("\nStarting server with host address:{} at port: {} and process id: {}\n"
              "If you wish to shutdown all clients type:---____-__-".format(host, port, os.getpid()))
        self.running_server()


    def running_server(self):
        # Start several server threads for processes that run in the background.
        initial_processes = [threading.Thread(target=self.server_connection_handler, args=()),
                             threading.Thread(target=self.test_connections, args=()),
                             threading.Thread(target=self.client_data_receiver, args=()),
                             ]
        for process in initial_processes:
            process.start()

        while True:
            # Keep requesting fastq files with full path, if multiple are send while one is being processed it is most
            #  likely blocked because test connection signals are send.
            file_location = input("\nPlease fill in a fastq file with full path to calculate the average value per base column wise.\n")
            total_clients = len(self.connection_list)
            fragments = self.chunker(file_location, total_clients)
            # Send data ranges of file.
            for index, connection in enumerate(self.connection_list):
                connection.send(pickle.dumps((fragments[index], file_location)))

    def test_connections(self):
        while True:
            # Delay sending data.
            time.sleep(0.5)
            for connection, address in zip(self.connection_list, self.address_list):
                try:
                    # Send data to test connection if no connection received it mentions the conn
                    connection.send("_----_".encode("ascii"))
                except (ConnectionResetError, BrokenPipeError):
                    print("Lost connection with: {}".format(address))
                    self.connection_list.remove(connection)
                    self.address_list.remove(address)
                    break

    def message_distribution(self):
        message = input("Give a script which can be executed: ")
        for connection in self.connection_list:
            connection.send(message.encode("ascii"))

    def server_connection_handler(self):
        while True:
            connection, address = self.setup_socket.accept()
            if len(self.connection_list) >= 3:
                print("Up to 3 connections are allowed.")
                connection.close()
            else:
                print("Connection from {}".format(address))
                self.connection_list.append(connection)
                self.address_list.append(address)

    def client_data_receiver(self):
        while True:
            time.sleep(1)
            for connection in self.connection_list:
                kaas = connection.recv(8192)
                if kaas == b"":
                    pass
                else:
                    counts, values = pickle.loads(kaas)
                    self.counted_list.append(counts)
                    self.value_list.append(values)
                    print(len(self.counted_list), len(self.connection_list))
                    if len(self.counted_list) == len(self.connection_list):
                        self.calculate_results(self.counted_list,self.value_list)
                    else:
                        pass



    def calculate_results(self,counted_occurrences, counted_values):
        counts = np.zeros(110)
        values = np.zeros(110)
        for index in range(len(counted_occurrences)):
            counts += counted_occurrences[index]
            values += counted_values[index]
        print((values/counts)-33)
        print("It took {} seconds.".format(time.time() - self.start))


    def chunker(self,input_file,requested_amount_of_fragments):
        chunks = []
        try:
            file_end = os.stat(input_file).st_size
            bytes_per_chunk = round(file_end / requested_amount_of_fragments)
            for chunk_start in range(0, file_end, bytes_per_chunk):
                chunks.append((chunk_start, chunk_start + bytes_per_chunk))
        except FileNotFoundError:
            print("There is no file with the name {}.".format(input_file))
        return chunks



def main(argv=None):
    if argv is None:
        argv = sys.argv
    MasterServer()
    return 0


if __name__ == "__main__":
    main()
